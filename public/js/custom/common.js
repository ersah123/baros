$(window).load(function() {
    if($(window).width() > 1024){
        $("#services-block").appear();
        $(document.body).on('appear', '#services-block', function(e, $affected) {
            // this code is executed for each appeared element
            $affected.addClass('appeared');
        });


        $("#project-box").appear();
        $(document.body).on('appear', '#project-box', function(e, $affected) {
            // this code is executed for each appeared element
            $affected.addClass('appeared');
        });

        $("#video-block").appear();
        $(document.body).on('appear', '#video-block', function(e, $affected) {
            // this code is executed for each appeared element
            $affected.addClass('appeared');
        });

        $("#about-block").appear();
        $(document.body).on('appear', '#about-block', function(e, $affected) {
            // this code is executed for each appeared element
            $affected.addClass('appeared');
        });


        $("#product-images-block").appear();
        $(document.body).on('appear', '#product-images-block', function(e, $affected) {
            // this code is executed for each appeared element
            $affected.addClass('appeared');
        });
       

        $("#call-to-action").appear();
        $(document.body).on('appear', '#call-to-action', function(e, $affected) {
            // this code is executed for each appeared element
            $affected.addClass('appeared');
        });

        $("#advantages-block").appear();
        $(document.body).on('appear', '#advantages-block', function(e, $affected) {
            // this code is executed for each appeared element
            $affected.addClass('appeared');
        });

        $.force_appear();
    }

    $("#download-cad").on('click', function() {
        $('.download-cad-popup-wrapper').addClass('popped');
        
    });

    $(document).mouseup(function(e) {
        var container = $(".download-cad-popup");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('.download-cad-popup-wrapper').removeClass('popped');
        }  
    });

    

    
});