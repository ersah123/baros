$(document).ready(function(){
  $(".owl-carousel-main").owlCarousel({
    items: 1,
    autoplay: true,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut'
  });

  var owlProjects = $(".owl-carousel-projects");
  owlProjects.owlCarousel({
    autoplay: true,
    nav: false,
    responsive: {
      0 : {
        items: 1,
      },
      600 : {
        items: 2,
      },
      1000 : {
        items: 3,
      },
      1200 : {
        items: 4,
      }
    }
  });


  if( $(window).width() > 768 ) {
    var owlProjects = $(".owl-carousel-products");
    owlProjects.owlCarousel({
      autoplay: true,
      nav: false,
      margin: 5,
      responsive: {
        0 : {
          items: 1,
        },
        600 : {
          items: 2,
        },
        1000 : {
          items: 3,
        },
        1200 : {
          items: 4,
        }
      }
    });
  }


  $(".owl-carousel-all-projects").owlCarousel({
    items: 1
  });

  $("#project-block .prev-action").on('click', function(){
    owlProjects.trigger('prev.owl.carousel');
  })

  $("#project-block .next-action").on('click', function(){
    owlProjects.trigger('next.owl.carousel');
  })


  if($( window ).width() < 768){
    $('.read-more').readmore({
      moreLink: '<a href="#">See more</a>',
      lessLink: '<a href="#">See less</a>',
      collapsedHeight: 68
    });
  } else {
    $('.read-more').readmore({
      moreLink: '<a href="#">See more</a>',
      lessLink: '<a href="#">See less</a>',
      collapsedHeight: 75
    });
  }

  

    // Or, hide them
    $("img").on("error", function() {
      $(this).hide();
  });

});

$(window).load(function() {
  $('#pre-loader').addClass('loaded');
});