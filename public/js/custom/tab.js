$(document).ready(function() {

  $("#tab1-content section").each(function(index) { // init
    var that = this;
    var t = setTimeout(function() { 
      $(that).addClass("animate-action"); 
    }, 100 * index);
  })


  $('#tabs li a').click(function(e) {
    var isActive = $(this).attr('class');
    if(isActive === 'active-title'){
      return;
    }
    var id = $(this).attr('id');
    $('.active-content section').each(function(index) {
      $(this).removeClass("animate-action"); 
    })
    $('.active-content, .active-title').removeClass('active-content');
    
    $('.active-title').removeClass('active-title');
    $('#tab-contents #'+id+'-content').addClass('active-content');
    $(this).addClass('active-title');  

    $('#tab-contents #'+id+'-content section').each(function(index) {
      var that = this;
      var t = setTimeout(function() { 
        $(that).addClass("animate-action"); 
      }, 100 * index);
    })
  });
});