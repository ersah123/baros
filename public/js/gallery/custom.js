$(document).ready(function(){
  $("#gallery").unitegallery({
    tile_enable_image_effect:true,
    tile_image_effect_type: "blur",
    tile_overlay_color: "#32647f",
    tile_overlay_opacity:0.4,
    tile_image_effect_reverse:true,
    tiles_col_width: 400,
    lightbox_show_textpanel: true,
    lightbox_textpanel_enable_title: true,
    lightbox_textpanel_enable_description: true
  });
});