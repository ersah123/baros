/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);
var i18n = require('i18n');
var _ = require('lodash');
var path = require('path')


// Add-in i18n support
keystone.pre('routes', i18n.init);



// Common Middleware
keystone.pre('render', middleware.flashMessages)
keystone.pre('render', middleware.initLanguage);
keystone.pre('routes', middleware.initLocals);
;


// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
};

// Setup Route Bindings
exports = module.exports = function (app) {

	// Views
	app.get('/', routes.views.index);
	app.get('/projects', routes.views.projects);
	app.get('/products/:category', routes.views.products);
	app.get('/product/:name', routes.views.product);
	app.get('/videos', routes.views.videos);
	app.get('/about', routes.views.about);
	app.get('/project/:name', routes.views.gallery);
	app.get('/products', routes.views.productCategories);
	// app.all('/contact', routes.views.contact);

	app.get('/change-lang/:lang',function (req, res) {
		res.cookie('lang' ,req.params.lang);
		if(req.get('Referrer')){
			res.redirect(302, req.get('Referrer'));
		} else {
			res.redirect(302, "http://localhost:3000/");
		}
	
	});

	  app.get('/download-cad/:productName/:format', function (req, res) {
		var format = req.params.format.toUpperCase();
		var product = req.params.productName.toUpperCase();

		var file = path.join(__dirname, '../public/Download-CAD/' + product + '.' + format);

		if (file) {
			res.download(file); // Set disposition and send it.
		  }
	  })

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);

};
