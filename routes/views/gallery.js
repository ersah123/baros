var keystone = require('keystone');
var replaceDashWithSpace = require('../utils/url');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'gallery';

	// Load the galleries by sortOrder
	view.query('galleries', keystone.list('Gallery').model.find({"key": req.params.name}).sort('sortOrder'));
	
	console.log('project', getProjectKey(req.params.name));

	// Render the view
	view.render('gallery', {
		projectName: getProjectKey(req.params.name),
		projectKey: getProjectKey(req.params.name) + "Text",
	});
};


function getProjectKey(projectSlug){ // replace project slug with camelCase
	return projectSlug.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });
}