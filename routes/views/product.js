var keystone = require('keystone');
var replaceDashWithSpace = require('../utils/url');
var fs = require('fs');



exports = module.exports = function (req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'product';
	
	console.log('project', removeSlug(req.params.name));

	var productFolder = './public/Download-CAD/'+ req.params.name;
	var files = [];

	if(fs.existsSync(productFolder)){
		files = fs.readdirSync(productFolder).map(filename => {
			return filename;
		});
	}
	

	console.log('files', files);
	console.log('files', getProductKey(req.params.name) + "Text3");

	// Render the view
	view.render('product', {
		productName: replaceDashWithSpace(req.params.name),
		productText1: getProductKey(req.params.name) + "Text1",
		productText2: getProductKey(req.params.name) + "Text2",
		productText3: getProductKey(req.params.name) + "Text3",
		projectImgFolder: removeSlug(req.params.name),
		productSlug: req.params.name,
		files: files
	} );
};

function getProductKey(productSlug) {
	return productSlug.replace(/-/g,'').toUpperCase();
}

function removeSlug(projectSlug){ // replace project slug with camelCase
	return projectSlug.replace(/-/g,'');
}