var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'products';

	// Render the view
	view.render('products', {
		productSlug: req.params.category,
		projectName: getProjectKey(req.params.category),
	});
};


function getProjectKey(projectSlug){ // replace project slug with camelCase
	return projectSlug.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });
}